import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class User {
    private String username;
    private String password;
    private String contactPhone;
    private String contactEmail;
    private String name;
    private String description;
    private Date joinDate;
    private List<Event> joiningEvents = new ArrayList<>();
    private List<Event> favEvents = new ArrayList<>();
    private boolean isAdmin = false;
    private boolean isVenue = false;
    private int penaltyCounter = 0;
    private List<ChatHead> chatList = new ArrayList<>();

    public User(String username, String password){
        super();
        this.username = username;
        this.password = password;

    }

    protected void setAdmin(){
        this.isAdmin = true;
    }

    protected String getUsername(){
        return this.username;
    }



    protected void joinEvent(Event event){
        if(this.penaltyCounter < 5){
            event.playerList.add(this);
        }
    }

    protected Event createEvent(String title, String location, int maxCount){
        Event event = new Event(title, location, maxCount, 1.2f, this);

        System.out.println("Event " + title +" has been created successfully");
        System.out.println("=============\nDetails:\n=============");
        System.out.println(event.getTitle());
        System.out.println("Hosted By: " + this.getUsername());
        System.out.println("Location: "+event.getLocation());
        System.out.println("Max Players: " + event.getMaxCount());
        System.out.println("Current Players: ");

        for(User player : event.getPlayerList()){
            if(!player.equals(-1)){
                System.out.println(player.getUsername());
            }else{
                System.out.println("null");
            }

        }
        return event;
    }

    protected void addEvent(Event event){
        this.joiningEvents.add(event);
    }



}
