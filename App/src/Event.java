import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Event {
    private String title;
    private Date eventDateTime;
    private String location;
    private User host;
    private String description;
    private float maxPower;
    private int currCount; //current amount of players
    private int minCount; //min amount of players
    private int maxCount; //max amount of players
    private boolean isCancelled = false;
    protected List<User> playerList = new ArrayList<User>();
    private List<Double> feeList = new ArrayList<Double>(); //list of fees that may or may not incur
    private List<String> tagList = new ArrayList<String>(); //list of tags

//    public Event(String title, Date eventDateTime, String location, int maxCount, User host){
    public Event(String title, String location, int maxCount,float maxPower, User host){
        super();
        this.host = host;
        this.title = title;
        this.maxPower = maxPower;
        this.location = location;
        this.maxCount = maxCount;
    }

    protected int getMaxCount(){
        return this.maxCount;
    }

    protected String getTitle(){
        return this.title;
    }

    protected String getLocation(){
        return this.location;
    }

    protected List<User> getPlayerList(){
        return this.playerList;
    }

    protected void setPower(float f){
        this.maxPower = f;
    }


    protected void addUser(User user){
        this.playerList.add(user);
        System.out.println("User "+ user.getUsername() + " has been added to the player list for the event!");
        user.addEvent(this);
    }


    protected void addTags(String tag){
        this.tagList.add(tag);
    }

    protected void addFees(double fee){
        this.feeList.add(fee);
    }

    //and so on
}
