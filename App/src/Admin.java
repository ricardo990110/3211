import java.util.List;

public class Admin extends User {



    public Admin(String username, String password) {
        super(username, password);
    }

    protected void deleteUser(List<User> users, User user){
        users.remove(user);
        System.out.println("Successfully deleted " + user.getUsername());
    }

    protected void promoteUser(User user) {
        user.setAdmin();
        System.out.println("Successfully promoted " + user.getUsername());
    }


}
